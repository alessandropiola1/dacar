/* global require */
var gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    sass = require('gulp-sass'),
    pug = require('gulp-pug'),
    notify = require("gulp-notify"),
    plumber = require("gulp-plumber"),
    concat = require('gulp-concat'),
    path = require('path'),
    data = require('gulp-data'),
    gutil = require('gutil'),
    ftp = require('vinyl-ftp'),
    javascriptObfuscator = require('gulp-javascript-obfuscator');

/* Change your directory and settings here */
var settings = {
    publicDir: '_staging/_site',
    sassDir: '_staging/_sass',
    sassIncludeDir: '_staging/_sass/_includes',
    layoutDir: '_staging/_layouts',
    partialsDir: '_staging/_layouts/_partials',
    dataDir: '_staging/_layouts/_data',
    cssDir: '_staging/_site/assets/css',
    jsDir: '_staging/_js',
    destjsDir: './_staging/_site/assets/js/',
    /* change to disable system notification */
    systemNotify: true
}

/**
 * serve task, will launch browserSync and launch index.html files,
 * and watch the changes for pug and sass files.
 **/
gulp.task('serve', ['sass'], function () {

    /**
     * Launch BrowserSync from publicDir
     */
    browserSync.init({
        server: settings.publicDir
    });

    /**
     * watch for changes in sass files 
     */
    gulp.watch(settings.sassDir + "**/**/*.sass", ['sass']);
    /**
     * watch for changes in js files 
     */
    gulp.watch(settings.jsDir + "/*.js", ['js']);

    /**
     * watch for changes in pug files 
     */
    gulp.watch([settings.layoutDir + "**/*.pug", settings.partialsDir + "/*.pug", settings.dataDir + "/*.pug.json"], ['pug']);

});

/**
 * sass task, will compile the .SCSS files,
 * and handle the error through plumber and notify through system message.
 */
gulp.task('sass', function () {
    return gulp.src(settings.sassDir + "/**/*.sass")
        .pipe(plumber({
            errorHandler: settings.systemNotify ? notify.onError("Error: <%= error.messageOriginal %>") : function (err) {
                console.log(" ************** \n " + err.messageOriginal + "\n ************** ");
                this.emit('end');
            }
        }))
        .pipe(sass())
        // .pipe(concat('style.css'))
        .pipe(gulp.dest(settings.cssDir))
        .pipe(browserSync.stream());
});

gulp.task('js', function () {
    return gulp.src(settings.jsDir + "/*.js")
        .pipe(javascriptObfuscator({
            compact: true
        }))
        .pipe(gulp.dest(settings.destjsDir))
        .pipe(browserSync.stream());
});

/**
 * pug task, will compile the pug,
 * and handle the error through plumber and notify through system message.
 */
gulp.task('pug', function () {
    return gulp.src(settings.layoutDir + "/*.pug")
        .pipe(data(function (file) {
            return require('./_staging/_layouts/_data/' + path.basename(file.path) + '.json');
        }))
        .pipe(plumber({
            errorHandler: settings.systemNotify ? notify.onError("Error: <%= error %>") : function (err) {
                console.log("************** \n " + err + "\n **************");
                this.emit('end');
            }
        }))
        .pipe(pug({
            pretty: true
        }))
        .pipe(gulp.dest(settings.publicDir))
        .pipe(browserSync.stream());
});



//copy file in prod
gulp.task('prod', function () {
    return gulp.src('./_staging/_site/**/*')
        .pipe(gulp.dest('./_prod'));
})

//deploy to remote server
var ftp_user = 'zbdacarm',
    ftp_pwd = 'quirfoobZedBewcev5',
    staging_files = ['./_staging/_site/**/*'],
    prod_files = ['./_prod/**/*'];

var staging_location = '/staging.dacarmilano.it',
    prod_location = './public_html';

function getFtpConnection() {
    return ftp.create({
        host: 'dacarmilano.it',
        port: 21,
        user: ftp_user,
        password: ftp_pwd,
        parallel: 5,
        log: gutil.log
    })
}
gulp.task('staging-ftp-upload', function () {
    var conn = getFtpConnection();
    return gulp.src(staging_files, { base: './_staging/_site', buffer: false })
        .pipe(conn.newer(staging_location))
        .pipe(conn.dest(staging_location))
})
gulp.task('prod-ftp-upload', function () {
    var conn = getFtpConnection();
    return gulp.src(prod_files, { base: './_prod', buffer: false })
        .pipe(conn.newer(prod_location))
        .pipe(conn.dest(prod_location))
})


/**
 * Default task, running just `gulp` will compile the sass,
 * compile the site, launch BrowserSync then watch
 * files for changes
 */


gulp.task('default', ['serve']);
