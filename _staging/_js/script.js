
$(document).ready(function () {

    // ---------- NAVBAR
    var $topNavMenu = $("#navbar .menu");
    var $topNavMenuLink = $("#navbar .link");
    $topNavMenu.on("click", function () {
        if ($(this).hasClass("open")) {
            $(this).removeClass("open");
            $topNavMenuLink.removeClass("open");
        }
        else {
            $(this).addClass("open");
            $topNavMenuLink.addClass("open");
        }
    });

    // ---------- HERO BANNER SLIDER
    // var heroSlider = new Swiper('#heroSlider', {
    //     preloadImages: false,
    //     lazy: true,
    //     loop: true,
    //     speed: 501,
    //     touchReleaseOnEdges: true,
    //     passiveListeners: true,
    //     autoHeight: true,
    //     navigation: {
    //         nextEl: '#heroSlider .swiper-button-next',
    //         prevEl: '#heroSlider .swiper-button-prev'
    //     },
    //     pagination: {
    //         el: '#heroSlider .swiper-pagination',
    //         clickable: true
    //     },
    //     autoplay: {
    //         delay: 4000,
    //         disableOnInteraction: false
    //     }
    // })

    // ---------- VIEW CAR DETAIL
    $("#homepageCars, #homepageCarsVeicoli").on("click", ".viewCar", function () {
        var dataVeicolo = $(this).data("veicolo")
        window.location.replace("/veicolo.html" + "?id-veicolo=" + dataVeicolo);
    });

    const observer = lozad(); // lazy loads elements with default selector as '.lozad'
    observer.observe();
});


// ---------- STICKY NAVBAR
$(window).scroll(function () {
    if ($(window).scrollTop() >= 1) {
        $('#navbar').addClass('sticky')
    }
    else {
        $('#navbar').removeClass('sticky')
    }
});

function getCarDetail(homepage) {
    const observer_swiper = lozad('.lozad-swiper');
    observer_swiper.observe();
    var array_fuel = { "references": [{ "id": "2", "name": "Elettrica/Benzina", "referenceType": "FuelCategory", "vehicleType": ["B", "C", "N", "X"] }, { "id": "3", "name": "Elettrica/Diesel", "referenceType": "FuelCategory", "vehicleType": ["C", "N", "X"] }, { "id": "B", "name": "Benzina", "referenceType": "FuelCategory", "vehicleType": ["B", "C", "N", "X"] }, { "id": "C", "name": "Metano", "referenceType": "FuelCategory", "vehicleType": ["C", "N", "X"] }, { "id": "D", "name": "Diesel", "referenceType": "FuelCategory", "vehicleType": ["B", "C", "N", "X"] }, { "id": "E", "name": "Elettrica", "referenceType": "FuelCategory", "vehicleType": ["B", "C", "N", "X"] }, { "id": "H", "name": "Idrogeno", "referenceType": "FuelCategory", "vehicleType": ["C", "N", "X"] }, { "id": "L", "name": "GPL", "referenceType": "FuelCategory", "vehicleType": ["B", "C", "N", "X"] }, { "id": "M", "name": "Etanolo", "referenceType": "FuelCategory", "vehicleType": ["C", "N", "X"] }, { "id": "O", "name": "Altro", "referenceType": "FuelCategory", "vehicleType": ["B", "C", "N", "X"] }, { "id": "T", "name": "Benzina 2T", "referenceType": "FuelCategory", "vehicleType": ["B"] }] }

    $.ajax({
        type: "GET",
        "headers": {
            "Authorization": "Basic aW5mb0BhbGVzc2FuZHJvcGlvbGEuaXQ6YXB2cnQxMG1p"
        },
        url: "https://listing-creation.api.autoscout24.com/customers/25701872/listings",
        success: function (data) {
            var numberOfCar = 0
            $.each(data["listings"], function (i, carId) {
                $.ajax({
                    type: "GET",
                    "headers": {
                        "Authorization": "Basic aW5mb0BhbGVzc2FuZHJvcGlvbGEuaXQ6YXB2cnQxMG1p"
                    },
                    url: "https://listing-creation.api.autoscout24.com/customers/25701872/listings/" + carId["id"],
                    success: function (auto) {
                        if (auto["publication"]["status"] === "Active") {
                            var power = Math.round(auto["power"] * 1.35962),
                                fuel = "",
                                name_model = auto["description"].split("\\\\"),
                                images = "";

                            $.each(array_fuel["references"], function (i, fuel_type) {
                                if (fuel_type["id"] == auto["fuelCategory"]) {
                                    fuel = fuel_type["name"]
                                }
                            })
                            $.each(auto["images"], function (index, image) {
                                images = images + "<div class='swiper-slide'><img class='lozad-swiper' data-src='" + image["previewUrl"] + "'/></div>"
                            })

                            numberOfCar++;
                            // console.log(numberOfCar)
                            if (homepage === true) {
                                if (numberOfCar <= 3) {
                                    $("#homepageCars").append("<div class='carContainer'> <div class='swiper-container lastCar id='" + carId["id"] + "'> <div class='swiper-button-prev'></div><div class='swiper-button-next'></div><div class='swiper-wrapper'><div class='swiper-slide'><img src='/assets/images/loader.gif'/></div>" + images + "</div></div><div class='infoAuto'> <p class='bold blue uppercase'>" + name_model[0].replace('**', '').replace('**', '') + "</p><div class='cont'> <div class='info info1'> <div><i class='fas fa-road'></i></div><div> <p>" + auto["mileage"] + " Km</p></div></div><div class='info info2'> <div><i class='fas fa-tachometer-alt'></i></div><div> <p>" + power + " CV</p></div></div></div><div class='cont'> <div class='info info1'> <div><i class='fas fa-gas-pump'></i></div><div> <p>" + fuel + "</p></div></div><div class='info info2'> <div><i class='fas fa-calendar-alt'></i></div><div> <p>" + auto["firstRegistrationDate"] + "</p></div></div></div><h3 class='blue'>" + auto["prices"]["public"]["price"] + "€</h3><a class='button button--blue viewCar' href='javascript:void(0);' data-veicolo='" + carId["id"] + "'>visualizza</a> </div></div>")
                                }
                            }
                            else {
                                $("#homepageCars").append("<div class='carContainer'> <div class='swiper-container lastCar id='" + carId["id"] + "'> <div class='swiper-button-prev'></div><div class='swiper-button-next'></div><div class='swiper-wrapper'><div class='swiper-slide'><img src='/assets/images/loader.gif'/></div>" + images + "</div></div><div class='infoAuto'> <p class='bold blue uppercase'>" + name_model[0].replace('**', '').replace('**', '') + "</p><div class='cont'> <div class='info info1'> <div><i class='fas fa-road'></i></div><div> <p>" + auto["mileage"] + " Km</p></div></div><div class='info info2'> <div><i class='fas fa-tachometer-alt'></i></div><div> <p>" + power + " CV</p></div></div></div><div class='cont'> <div class='info info1'> <div><i class='fas fa-gas-pump'></i></div><div> <p>" + fuel + "</p></div></div><div class='info info2'> <div><i class='fas fa-calendar-alt'></i></div><div> <p>" + auto["firstRegistrationDate"] + "</p></div></div></div><h3 class='blue'>" + auto["prices"]["public"]["price"] + "€</h3><a class='button button--blue viewCar' href='javascript:void(0);' data-veicolo='" + carId["id"] + "'>visualizza</a> </div></div>")
                            }


                            var swiperImagesAuto = new Swiper(".lastCar", {
                                preloadImages: false,
                                lazy: true,
                                loop: true,
                                touchReleaseOnEdges: true,
                                passiveListeners: true,
                                autoHeight: false,
                                navigation: {
                                    nextEl: '.lastCar .swiper-button-next',
                                    prevEl: '.lastCar .swiper-button-prev'
                                }
                            });

                            setCarTextHeight()

                        }
                    },
                    complete: function () {
                        observer_swiper.observe();
                        setTimeout(() => {
                            $("img[src='/assets/images/loader.gif']").parents(".swiper-slide").remove();
                        }, 1000);
                    }
                });
            });
        },
        complete: function () {
            $(".loading-dacar").hide()
        }
    });
}

function setCarTextHeight() {
    var highHeight = "0";
    $(".carContainer .infoAuto .bold.blue.uppercase").each(function () {
        var thHeight = $(this).height();
        if (highHeight < thHeight) {
            highHeight = thHeight;
        }
    });
    $(".carContainer .infoAuto .bold.blue.uppercase").css('height', highHeight)
}
$(window).on('load', function () {
    setCarTextHeight()
})
$(window).on('resize', function () {
    setCarTextHeight()
})
